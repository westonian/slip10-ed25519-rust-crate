[![Build Status](https://travis-ci.com/westonian/slip10-ed25519-rust-crate.svg?branch=master)](https://travis-ci.com/westonian/slip10-ed25519-rust-crate)

Derive private keys for the ed25519 curve for [SLIP-0010](https://github.com/satoshilabs/slips/blob/master/slip-0010.md).
